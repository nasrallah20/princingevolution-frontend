package com.pricinghub.pricingevolution.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDate;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompetitorPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String competitor;
    private LocalDate date;
    private Double price;

}
