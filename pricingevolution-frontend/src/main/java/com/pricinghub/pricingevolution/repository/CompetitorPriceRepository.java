package com.pricinghub.pricingevolution.repository;

import com.pricinghub.pricingevolution.model.CompetitorPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetitorPriceRepository extends JpaRepository<CompetitorPrice, String> {

    List<CompetitorPrice> findCompetitorPriceByCompetitorOrderByDateAsc(String competitor);

    @Query("SELECT DISTINCT(c.competitor) FROM CompetitorPrice c")
    List<String> findAllCompetitorsPrice();
}
