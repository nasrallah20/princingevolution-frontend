package com.pricinghub.pricingevolution.utils;

import com.pricinghub.pricingevolution.model.CompetitorPrice;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class ReadDataFile {

    public static List<CompetitorPrice> readRecords() {

        List<CompetitorPrice> prices = new ArrayList<>();
        Path pathToFile = Paths.get("data.csv");

        try (BufferedReader br = Files.newBufferedReader(pathToFile,
                StandardCharsets.US_ASCII)) {

            br.readLine();
            String line = br.readLine();

            while (line != null) {
                String[] attributes = line.split(",");
                CompetitorPrice price = createPrice(attributes);
                prices.add(price);
                line = br.readLine();
                System.out.println(price.getCompetitor());
                System.out.println(price.getDate());
                System.out.println(price.getPrice());
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return prices;
    }

    private static CompetitorPrice createPrice(String[] attributes) {
        CompetitorPrice competitorPrice = new CompetitorPrice();
        competitorPrice.setCompetitor(attributes[2]);
        competitorPrice.setDate(toLocalDate(attributes[0]));
        competitorPrice.setPrice(Double.parseDouble(attributes[1]));
        return competitorPrice;
    }

    private static LocalDate toLocalDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(date, formatter);
    }

}
