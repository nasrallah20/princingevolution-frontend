package com.pricinghub.pricingevolution.utils;

import com.pricinghub.pricingevolution.repository.CompetitorPriceRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import static com.pricinghub.pricingevolution.utils.ReadDataFile.readRecords;

@Configuration
@Order(0)
@RequiredArgsConstructor
public class DataPopulate {

    private final CompetitorPriceRepository repository;

    @PostConstruct
    public void createDataFromCsv(){
        repository.deleteAll();
        repository.saveAll(readRecords());
    }
}
