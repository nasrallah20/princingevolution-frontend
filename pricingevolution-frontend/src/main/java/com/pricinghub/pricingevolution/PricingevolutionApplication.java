package com.pricinghub.pricingevolution;

import com.pricinghub.pricingevolution.service.CompetitorPriceService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PricingevolutionApplication {

	public static void main(String[] args) {

		SpringApplication.run(PricingevolutionApplication.class, args);

	}

}
