package com.pricinghub.pricingevolution.service;

import com.pricinghub.pricingevolution.model.CompetitorPrice;
import com.pricinghub.pricingevolution.repository.CompetitorPriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
public class CompetitorPriceServiceImpl implements CompetitorPriceService{

    @Autowired
    CompetitorPriceRepository repository;


    @Override
    public List<CompetitorPrice> findPricesByCompetitor(String competitor) {
        return repository.findCompetitorPriceByCompetitorOrderByDateAsc(competitor);
    }

    @Override
    public List<String> findAllCompetitors() {
        return repository.findAllCompetitorsPrice();
    }


}
