package com.pricinghub.pricingevolution.service;

import com.pricinghub.pricingevolution.model.CompetitorPrice;

import java.util.List;

public interface CompetitorPriceService {

    List<CompetitorPrice> findPricesByCompetitor(String competitor);

    List<String> findAllCompetitors();

}
