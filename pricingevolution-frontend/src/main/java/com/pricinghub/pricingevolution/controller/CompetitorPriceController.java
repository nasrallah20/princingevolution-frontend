package com.pricinghub.pricingevolution.controller;

import com.pricinghub.pricingevolution.model.CompetitorPrice;
import com.pricinghub.pricingevolution.service.CompetitorPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/competitorPrice")
public class CompetitorPriceController {

    @Autowired
    CompetitorPriceService service;

    @GetMapping("/allPricesForCompetitor")
    public List<CompetitorPrice> getCompetitorsPriceByDate(@RequestParam String competitor){
        return service.findPricesByCompetitor(competitor);
    }

    @GetMapping("/allCompetitors")
    public List<String> getCompetitors(){
        return service.findAllCompetitors();
    }
}
