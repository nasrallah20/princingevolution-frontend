# Getting Started with Pricing Evolution App

## Context
This application helps us visualize the evolution of prices on a graph. The user can choose the competitors form whom whe would like to see the prices evolution and can also choose the interval of dates where the prices evolved.

This project is composed of two parts :
- pricingevolution :
It represents the BackEnd part of the application

- pricingevolution-frontend :
Which represents the FrontEnd part of the application


## BackEnd
The BackEnd application was developped with Java as well as Spring Boot.
The application was initialized with SpringBoot initlialize at this link : https://start.spring.io/.
On the initialization, I chose a Maven Project, with, as mentioned before, Java 17, and I added theses dependencies :

- Spring Web
- Spring Data JPA
- MySQL Driver

After generating the project, a zip is downloaded with the application ready to start development.

### Setting up DataBase
For the database, I chose to work with SQL databases because I know well how to use it.
All the dependencies were already added by SpringBoot initializer, all I had to do was to add the configuration of my database in the application.properties file with the url, username and password of my database.
I used PhpMyAdmin to visualize my data.
The data was in a CSV file, so I chose to transform it into my Java Object, this way i would be able to stock the Date data as a Date Object which will facilitate the interrogation of the database, especially that we will have queries with dates.
To populate the database, I used a two classes in the "utils" directory 
- ReadDataFile.java : a class that will read the csv file and add each data in a list as a Java Object.
- DataPopulate.java : a class that will be run each time we run the application. With Spring, it can be easily done by adding the annotations :
"\@Configuration
\@Order(0)"
I did this in case we change the data in the database, to make sure that every time we restart the application with the same data.

### Adding the entity
CompetitorPrice.java:
I added an Entity based on the data in the csv file which contains the 3 parameters and I added the annotation @Entity that helps Spring knowing that this is a Data Object.

### Addinng the repository
CompetitorPriceRepository.java
I added the Repository interface that extends the JPA repository that allows us to interrogate the database easily using the @Repository annotation.

### Adding the service
Interface : CompetitorPriceService.java and implementation : CompetitorPriceServiceImpl
These classes will help us do the connection between the apis and the database.
We will find two services, one to populate the dropdown list in the FrontEnd so the user can choose the list of competitors he wants to see, and the other one is to get the data of prices based on the selection of the user in the FrontEnd.

### Adding Controller
In the controller, we will have the different apis tha use the different services provided to get the data needed.

### Run the server
To run this application, we can start by running the command :
`mvn clean install`
then run the application based on the IDE we are using
(I used IntelliJ)

To test if my apis worked as expected before starting to develop the FrontEnd, I used Postman.

## FrontEnd
To initialize the FrontEnd application I ran the command below :
`npx create-react-app pricingevolution-frontend`
I worked with the version 16 of node and the version 9 of npm.
To use the charts library I installed the library react-chartjs-2.

### Application structure
In the FrontEnd application we can find 3 components :
- App.js : Which is the generic component that will contain the other ones
- AppSelectionForm.js : Which is the component that contain the form with the different fields and the button to do out search.
- AppResults.js : Which is the component that will diplay the data in a chart based on the request of the user. This component is the Child component of the AppSelectionForm.js because there is exchanging between the two.

### Using the BackEnd
In the AppSelectionForm component, I used the first api which is :
/api/competitorPrice/allCompetitors 
This api will fetch the data in the dropdown. For the dropdown I used the library ReactSelect that allows us to have checkboxes in the dropdown to select multiple competitors.
For the date pickers, I used the library Material UI which is very popular in react application, as well as for the disposition of the different elements on the page.
Finally a "Submit" button that will call the second api which is :
/api/competitorPrice/allPricesForCompetitor
and will take as a parameters the start date, the end date and the competitors chosen by the user.
This data will then be sent to the child component AppResults, and we will use to create our chart.

I achieved the propagation of the data as well as creating the chart with the necessary data, but I couldn't diplay the chart. I will explain the problem next.

### Problems encountered
Sadly, I didn't get to represent the charts because I had this error :

```
Missing class properties transform.
566 | };
567 | class DatasetController {

568 | static defaults = {};
| ^^^^^^^^^^^^^^^^^^^^^
569 | static datasetElementType = null;
570 | static dataElementType = null;
571 | constructor(chart, datasetIndex){
at transformFile.next ()
at run.next ()
at transform.next ()
```

After a lot of searches, I came to the conclusion that there might be a conflict with the versions of chart.js and react-chartjs-2 which are both used to create the charts.
Sadly, I don't have enough time to search more for the solutions even though I tried some manipulation to resolve the problem.

### Run the application

To run the application, start by installing all the node modules. To do so, run this command :
`npm install`

Then, when all the modules are installed, you can run the application with this command :
`npm start`

I diplayed the data in the console.log so you could at least see the results of my work.

# Thank you !
