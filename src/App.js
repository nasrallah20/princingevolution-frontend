import * as React from 'react';
import './App.css';
import Appbar from './components/Appbar';
import AppSelectionForm from './components/AppSelectionForm';


function App() {
  return (
    <div className="App">
      <Appbar/>
      <AppSelectionForm/>
    </div>
  );
}

export default App;
