import * as React from 'react';
import {Grid} from '@mui/material';

//import { useEffect, useRef } from 'react';
//import Chart from 'chart.js';

export default function AppResults(props) {

  //P.S. : EVERYTHING THAT HAS TO DO WITH DISPLAYING THE CHART IS COMMENTED
  //OTHERWISE THE COMPILATION FAILS

  //const chartRef = useRef();

  let resultByCompetitor = new Map();

  function getDataset(data, label) { 
    return { 
      label: label, 
      fillColor: 'rgba(220,220,220,0.2)', 
      strokeColor: 'rgba(220,220,220,1)', 
      pointColor: 'rgba(220,220,220,1)', 
      pointStrokeColor: '#fff', 
      pointHighlightFill: '#fff', 
      pointHighlightStroke: 'rgba(220,220,220,1)', 
      data: data.price
    }; 
  }
  
  props.data.forEach((obj) => {
    if (resultByCompetitor.has(obj.competitor)) {
      const current = resultByCompetitor.get(obj.competitor);
      current.date.push(obj.date);
      current.price.push(obj.price);
      resultByCompetitor.set(obj.competitor, current);
    } else {
      resultByCompetitor.set(obj.competitor, { date: [obj.date], price: [obj.price] });
    }
  });

  let dataSetsToAdd = [];
  resultByCompetitor.forEach((dataByCompetitor, competitor) =>  { 
    const label = `${competitor}`
    dataSetsToAdd.push(getDataset(dataByCompetitor, label));
  });

  let chartData = {
    labels : resultByCompetitor.values().next().value.date,
    datasets : dataSetsToAdd
  };
  console.log("chartData : ", chartData);

  /*
  useEffect(() => {
    const myChartRef = chartRef.current.getContext('2d');

    new Chart(myChartRef, {
      type: 'line',
      data: chartData,
    });
  }, [chartData]);
  */

  return (
    <div>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={12} sm={6}>
          <h3>ALL DATA FROM DATABASE</h3>
          {props.data.map(item => (
            <p key={item.id}>{item.competitor} - {item.price} - {item.date}</p>
          ))}
        </Grid>
        <Grid item xs={12} sm={6}>
          <h3>STRUCTURE OF CHART OBJECT</h3>
          <pre>{JSON.stringify(chartData, null, 2)}</pre>
        </Grid>
      </Grid>
    </div>  
  );
}
