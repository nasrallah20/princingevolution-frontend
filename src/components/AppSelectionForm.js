import * as React from 'react';
import {useState, useEffect} from 'react';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { Grid, Button } from '@mui/material';
import { default as ReactSelect } from "react-select";
import { components } from "react-select";
import AppResults from './AppResults';


export default function AppSelectionForm() {
    const buttonStyle = {
        marginTop: '10px'
    };

    const datePickerStyle = {
        marginLeft: '10px'
    }

    const optionsStyle = {
        marginRight: '10px'
    }

    const[startDate, setStartDate] = useState(null);
    const[endDate, setEndDate] = useState(null);
    const[data, setData] = useState([]);
    const[selectedCompetitors, setSelectedCompetitors] = useState([]);
    const[chartResults, setChartResults] = useState(null);

    const Option = (props) => {
        return (
          <div>
            <components.Option {...props}>
              <input
                type="checkbox"
                checked={props.isSelected}
                onChange={() => null}
              />{" "}
              <label>{props.label}</label>
            </components.Option>
          </div>
        );
      };

    useEffect( () => {
         const fetchData = async () => {
            try{
                const response = await fetch("http://localhost:8080/api/competitorPrice/allCompetitors");
                const json = await response.json();
                setData(json)
            } catch (error){
                console.log(error)
            }
         };
         fetchData();
    }, [])

    const competitorOptions = data.map(item => {
        return {
            value: item,
            label: item
        }
    });
    
    const handleClick = async () => {
        if (startDate && endDate && startDate <= endDate) {
            const selectedCompetitorsString = selectedCompetitors.map(selectedCompetitor => `competitors=${selectedCompetitor.label}`).join('&');
            const startDateObject = new Date(startDate);
            const endDateObject = new Date(endDate);
            const formattedStartDate = startDateObject.toISOString().substring(0,10);
            const formattedEndDate = endDateObject.toISOString().substring(0,10);

            const response = await fetch(`http://localhost:8080/api/competitorPrice/allPricesForCompetitor?startDate=${formattedStartDate}&endDate=${formattedEndDate}&${selectedCompetitorsString}`);
            const responseData = await response.json();
            setChartResults(responseData);
        } else {
            alert('Start date should be before end date')
        }
    }

  return (
    <div>
        <form
        noValidate
        autoComplete="off">
            <Grid container spacing={3} alignItems="center">
                <Grid item xs={12} sm={4}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DemoContainer components={['DatePicker']}>
                    <DatePicker label="Start date" 
                    value={startDate}
                    dateFormat="yyyy-MM-dd"
                    style={datePickerStyle}
                    onChange={(e) => setStartDate(e)}/>
                    </DemoContainer>
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DemoContainer components={['DatePicker']}>
                            <DatePicker label="End date" 
                            value={endDate}
                            onChange={(e) => setEndDate(e)}/>
                        </DemoContainer>
                    </LocalizationProvider>
                </Grid>
                <Grid  item xs={12} sm={4}>
                    <ReactSelect
                        options={competitorOptions}
                        isMulti
                        closeMenuOnSelect={false}
                        hideSelectedOptions={false}
                        style={optionsStyle}
                        components={{
                            Option
                        }}
                        value={selectedCompetitors}
                        onChange={(e) => setSelectedCompetitors(e)}
                        allowSelectAll={true}
                    />
                </Grid>
            </Grid>
            <Grid>
                <Button variant="contained"
                onClick={handleClick}
                style={buttonStyle}>Submit</Button>
            </Grid> 
        </form>
        {chartResults && <AppResults data={chartResults} />}
   </div>
  );
}